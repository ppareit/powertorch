package be.locpiapps.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by ppareit on 25/08/15.
 */
public class BroadcastReceiverUtils {

    public static BroadcastReceiver createBroadcastReceiver(OnReceiveListener onReceiverListener) {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onReceiverListener.onReceive(context, intent);
            }
        };
    }

    public interface OnReceiveListener {
        public void onReceive(Context context, Intent intent);
    }

}

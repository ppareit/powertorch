package be.locpiapps.android;

import android.os.AsyncTask;

/**
 * Created by ppareit on 20/08/15.
 */
public class BackgroundTask {

    public BackgroundTask(Runnable runnable) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                runnable.run();
                return null;
            }
        }.execute();

    }
}

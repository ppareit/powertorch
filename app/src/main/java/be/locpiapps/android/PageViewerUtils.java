package be.locpiapps.android;

import androidx.viewpager.widget.ViewPager;

/**
 * Created by ppareit on 21/08/15.
 */
public class PageViewerUtils {

    public static void addOnPageSelectedListener(ViewPager viewPager, OnPageSelectedListener onPageSelectedListener) {
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                                              @Override
                                              public void onPageSelected(int position) {
                                                  onPageSelectedListener.onPageSelected(position);
                                              }
                                          }
        );
    }

    public interface OnPageSelectedListener {
        public void onPageSelected(int position);
    }

}

package be.locpiapps.android;

import android.content.Context;
import android.widget.Toast;

public class ToastBuilder {

    Context mContext = null;
    String mText = null;
    int mResId = 0;
    int mDuration = 0;

    public ToastBuilder(Context context) {
        mContext = context.getApplicationContext();
    }

    public ToastBuilder setText(String text) {
        mText = text;
        return this;
    }

    public ToastBuilder setText(int resId) {
        mText = mContext.getResources().getString(resId);
        return this;
    }

    public enum Duration {
        SHORT(Toast.LENGTH_SHORT), LONG(Toast.LENGTH_LONG);
        private final int mDuration;

        Duration(int duration) {
            mDuration = duration;
        }

        int getDuration() {
            return mDuration;
        }
    };

    public ToastBuilder setDuration(Duration duration) {
        mDuration = duration.getDuration();
        return this;
    }

    public Toast build() {
        return Toast.makeText(mContext, mText, mDuration);
    }

}

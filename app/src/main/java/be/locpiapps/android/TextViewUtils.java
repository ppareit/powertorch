package be.locpiapps.android;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by ppareit on 21/08/15.
 */
public class TextViewUtils {

    public static void addAfterTextChangedListener(TextView textView, AfterTextChangedListener listener) {
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                listener.afterTextChanged(s);
            }
        });
    }

    public interface AfterTextChangedListener {
        public void afterTextChanged(Editable s);
    }

}

package be.locpiapps.android;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import android.util.Log;

public class AppBase extends Application {

    private static final String TAG = AppBase.class.getSimpleName();

    private static Context sContext;

    /**
     * @return the Context of this application
     */
    public static Context getAppContext() {
        if (sContext == null)
            Log.e(TAG, "Global context not set");
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate called");
        sContext = getApplicationContext();
    }

    /**
     * @return true if this is the demo version
     */
    public static boolean isDemoVersion() {
        try {
            return sContext.getPackageName().contains("demo");
        } catch (Exception swallow) {
        }
        return false;
    }

    /**
     * @return true if this is the paid version
     */
    public static boolean isPaidVersion() {
        try {
            return sContext.getPackageName().contains("demo");
        } catch (Exception swallow) {
        }
        return false;
    }

    /**
     * @return true if this is the very first time this function is ever called
     */
    public static boolean isFirstTimeStarted() {
        Context context = getAppContext();
        if (context == null) {
            Log.e(TAG, "Failed to get a valid context");
            return false;
        }
        // @BUG: this might be called a couple of times on the first run and
        // break
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        boolean firsttime = sp.getBoolean("is_firsttime_started", true);
        if (firsttime) {
            sp.edit().putBoolean("is_firsttime_started", false).apply();
        }
        return firsttime;
    }

    /**
     * Get the version name from the manifest.
     * 
     * @return The version as a String.
     */
    public static String getVersionName() {
        Context context = getAppContext();
        String packageName = context.getPackageName();
        try {
            PackageManager pm = context.getPackageManager();
            return pm.getPackageInfo(packageName, 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Unable to find the name " + packageName
                    + " in the package");
            return null;
        }
    }

    /**
     * Get the version code from the manifest.
     * 
     * @return The version as a int.
     */
    public static int getVersionCode() {
        Context context = getAppContext();
        String packageName = context.getPackageName();
        try {
            PackageManager pm = context.getPackageManager();
            return pm.getPackageInfo(packageName, 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Unable to find the name " + packageName
                    + " in the package");
            return -1;
        }
    }

}

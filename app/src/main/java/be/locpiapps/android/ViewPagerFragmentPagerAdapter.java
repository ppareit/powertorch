package be.locpiapps.android;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.SparseArray;
import android.view.ViewGroup;

import static be.locpiapps.android.PageViewerUtils.addOnPageSelectedListener;

/**
 * Created by ppareit on 24/08/15.
 */
abstract public class ViewPagerFragmentPagerAdapter  extends FragmentPagerAdapter {

    SparseArray<ViewPagerFragment> registeredFragments = new SparseArray<>();
    int previousPosition = -1;


    public ViewPagerFragmentPagerAdapter(FragmentManager fm, ViewPager viewPager) {
        super(fm);
        addOnPageSelectedListener(viewPager,(position)->{
            if (previousPosition != -1) {
                ViewPagerFragment previousFragment = getRegisteredFragment(previousPosition);
                if (previousFragment != null) previousFragment.onPageUnselected();
            }
            ViewPagerFragment currentFragment = getRegisteredFragment(position);
            if (currentFragment != null) currentFragment.onPageSelected();

            previousPosition = position;
        });
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewPagerFragment fragment = (ViewPagerFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public ViewPagerFragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}

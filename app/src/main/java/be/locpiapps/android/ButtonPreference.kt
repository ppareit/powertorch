package be.locpiapps.android

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import be.locpiapps.powertorch.R

class ButtonPreference : Preference {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        buttonText = attrs?.getAttributeValue(null, "buttonText") ?: ""
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        buttonText = attrs?.getAttributeValue(null, "buttonText") ?: ""
    }

    init {
        widgetLayoutResource = R.layout.button_preference_widget_layout
    }

    private var button: Button? = null


    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)

        button = holder.findViewById(R.id.button) as Button
        button?.text = buttonText
        button?.setOnClickListener(onButtonClickListener)
    }

    var buttonText: CharSequence = ""
        set(value) {
            field = value
            button?.text = value
        }

    var onButtonClickListener: View.OnClickListener = View.OnClickListener { }
        set(value) {
            field = value
            button?.setOnClickListener(value)
        }

}
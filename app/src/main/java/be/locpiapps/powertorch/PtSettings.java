package be.locpiapps.powertorch;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.vrallev.android.cat.Cat;

public class PtSettings {

    public static boolean getConsentForStrobe() {
        final SharedPreferences sp = getSharedPreferences();
        return sp.getBoolean("consent_for_strobe", false);
    }

    public static void setConsentForStrobeToTrue() {
        final SharedPreferences sp = getSharedPreferences();
        sp.edit().putBoolean("consent_for_strobe", true).apply();
    }

    public static boolean enablePowerButtonToggle() {
        try {
            final SharedPreferences sp = getSharedPreferences();
            return sp.getBoolean("powerbutton_preference", true);
        } catch (NullPointerException swallow) {
            // We get a lot of null pointer exceptions here.
            // Quiting the app would be best I suppose?
            return false;
        }
    }

    public enum VibrateSetting {
        NONE, SHORT, LONG, CONTINUES,
    }

    public static VibrateSetting getVibrateSetting() {
        final SharedPreferences sp = getSharedPreferences();
        String vib = sp.getString("vibrate_preference", "SHORT");
        switch (vib) {
            case "NONE":
                return VibrateSetting.NONE;
            case "SHORT":
                return VibrateSetting.SHORT;
            case "LONG":
                return VibrateSetting.LONG;
            case "CONTINUES":
                return VibrateSetting.CONTINUES;
            default:
                Cat.e("Vibrate setting is messed up, returning default");
                return VibrateSetting.SHORT;
        }
    }

    /**
     * @return timeout for torch in milliseconds
     */
    public static long getTimeout() {
        final SharedPreferences sp = getSharedPreferences();
        String timeoutValue = sp.getString("timeout_preference", "120");
        long timeout = Long.valueOf(timeoutValue);
        timeout *= 1000; // move to milliseconds
        return timeout;
    }

    /**
     * @return true if the torch should timeout after a while
     */
    public static boolean shouldTimeout() {
        return getTimeout() > 5000; // take at least 5 seconds
    }

    /**
     * @return true if we should ignore the power button presses when the device
     *         is charging
     */
    public static boolean ignorePowerButtonWhenCharging() {
        final SharedPreferences sp = getSharedPreferences();
        return sp.getBoolean("ignorewhencharging_preference", true);
    }

    /**
     * @return the SharedPreferences for this application
     */
    public static SharedPreferences getSharedPreferences() {
        final Context context = App.getAppContext();
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}

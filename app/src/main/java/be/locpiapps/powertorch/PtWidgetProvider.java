package be.locpiapps.powertorch;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;


public class PtWidgetProvider extends AppWidgetProvider {

    private final static String TAG = "PtWidgetProvider";


    public static class UpdateService extends Service {
        @Override
        public int onStartCommand(final Intent intent, final int flags, final int startId) {
            Log.d(PtWidgetProvider.TAG, "UpdateService start command");
            String _xifexpression = null;
            boolean _isOn = TorchService.isTorchOn();
            if (_isOn) {
                _xifexpression = TorchService.ACTION_STOP_TORCH;
            } else {
                _xifexpression = TorchService.ACTION_START_TORCH;
            }
            final String action = _xifexpression;
            final Intent startIntent = new Intent(action);
            final PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, startIntent, 0);
            String _packageName = this.getPackageName();
            final RemoteViews views = new RemoteViews(_packageName, R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.widget_button, pendingIntent);
            int _xifexpression_1 = (int) 0;
            boolean _isOn_1 = TorchService.isTorchOn();
            if (_isOn_1) {
                _xifexpression_1 = R.drawable.widget_flashlight;
            } else {
                _xifexpression_1 = R.drawable.widget_off;
            }
            final int drawable = _xifexpression_1;
            views.setImageViewResource(R.id.widget_button, drawable);
            final AppWidgetManager manager = AppWidgetManager.getInstance(this);
            final ComponentName widget = new ComponentName(this, PtWidgetProvider.class);
            manager.updateAppWidget(widget, views);
            return Service.START_NOT_STICKY;
        }

        @Override
        public IBinder onBind(final Intent arg0) {
            return null;
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        String _action = intent.getAction();
        String _plus = ("Received broadcast: " + _action);
        Log.v(PtWidgetProvider.TAG, _plus);
        final String action = intent.getAction();
        boolean _or = false;
        boolean _equals = action.equals(TorchService.TORCH_STARTED);
        if (_equals) {
            _or = true;
        } else {
            boolean _equals_1 = action.equals(TorchService.TORCH_STOPPED);
            _or = _equals_1;
        }
        if (_or) {
            final Intent updateIntent = new Intent(context, PtWidgetProvider.UpdateService.class);
            context.startService(updateIntent);
        }
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
        Log.d(PtWidgetProvider.TAG, "updated called");
        final Intent intent = new Intent(context, PtWidgetProvider.UpdateService.class);
        context.startService(intent);
    }

}

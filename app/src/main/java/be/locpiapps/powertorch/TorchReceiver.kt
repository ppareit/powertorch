package be.locpiapps.powertorch

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import org.koin.java.KoinJavaComponent.inject

class TorchReceiver() : BroadcastReceiver() {

    enum class Action(val actionName: String) {
        STOP_TORCH("be.ppareit.powertorch.ACTION_STOP_TORCH"),
        START_TORCH("be.ppareit.powertorch.ACTION_START_TORCH"),
        TOGGLE_TORCH("be.ppareit.powertorch.ACTION_TOGGLE_TORCH"),
        START_STROBE("be.ppareit.powertorch.ACTION_START_STROBE");

        companion object {
            fun fromActionName(actionName: String): Action =
                values().first { it.actionName == actionName }
        }
    }

    init {
        val filter = IntentFilter().apply {
            Action.values().forEach { addAction(it.actionName) }
        }
        appContext.registerReceiver(this, filter)
    }

    companion object {
        private val appContext: Context by inject(Context::class.java)
        fun createPendingIntent(action: Action): PendingIntent {
            val intent = Intent(action.actionName)
            return PendingIntent.getBroadcast(
                appContext, 0,
                intent, FLAG_IMMUTABLE
            )
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        when (Action.fromActionName(intent.action.orEmpty())) {
            Action.STOP_TORCH -> torch.turnTorchOff()
            Action.START_TORCH -> torch.changeTorchMode(Torch.Mode.FLASH_LIGHT)
            Action.TOGGLE_TORCH -> torch.toggleTorch()
            Action.START_STROBE -> torch.changeTorchMode(Torch.Mode.STROBE)
        }
    }
}
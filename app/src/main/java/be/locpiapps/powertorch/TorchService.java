package be.locpiapps.powertorch;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.vrallev.android.cat.Cat;

/**
 * Created by ppareit on 18/08/17.
 */

public class TorchService extends Service {
    // these broadcasts can be used as intents to control the torch
    public static final String ACTION_STOP_TORCH = "be.ppareit.powertorch.ACTION_STOP_TORCH";
    public static final String ACTION_START_TORCH = "be.ppareit.powertorch.ACTION_START_TORCH";
    public static final String ACTION_START_STROBE = "be.ppareit.powertorch.ACTION_START_STROBE";

    // these broadcasts are send when torch state changes
    public static final String TORCH_STARTED = "be.ppareit.powertorch.TORCH_STARTED";
    public static final String TORCH_STOPPED = "be.ppareit.powertorch.TORCH_STOPPED";
    public static final String TORCH_UPDATED = "be.ppareit.powertorch.TORCH_UPDATED";

    public enum TorchMode {
        FLASH_LIGHT,
        STROBE,
        BEACON,
        MOONLIGHT,
        SOS
    }

    private static TorchService INSTANCE;

    CameraManager cameraManager = null;
    private boolean isTorchOn = false;
    private TorchMode torchMode = TorchMode.FLASH_LIGHT;

    CameraManager.TorchCallback torchCallback = new CameraManager.TorchCallback() {
        @Override
        public void onTorchModeChanged(@NonNull String cameraId, boolean enabled) {
            Cat.d("Torch mode changed: enabled = " + enabled);

            if (torchMode == TorchMode.FLASH_LIGHT) {
                isTorchOn = enabled;
                sendBroadcast(new Intent(enabled ? TORCH_STARTED : TORCH_STOPPED));
            } else if (torchMode == TorchMode.STROBE) {
                if (isTorchOn) {
                    try {
                        if (enabled) {
                            cameraManager.setTorchMode(getCameraIdWithFlash(), false);
                            Thread.sleep(90);
                        } else {
                            cameraManager.setTorchMode(getCameraIdWithFlash(), true);
                            Thread.sleep(10);
                        }
                    } catch (CameraAccessException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        @Override
        public void onTorchModeUnavailable(@NonNull String cameraId) {
            Cat.d("Torch mode no longer available, stopping torch");
            isTorchOn = false;
            sendBroadcast(new Intent(TORCH_STOPPED));
        }
    };

    public static class StopReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Cat.d("onReceive: " + intent.getAction());
            turnTorchOff();
        }
    }

    public static class StartReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Cat.d("onReceive: " + intent.getAction());
            switch (intent.getAction()) {
                case ACTION_START_TORCH:
                    turnTorchOn(TorchMode.FLASH_LIGHT);
                    break;
                case ACTION_START_STROBE:
                    turnTorchOn(TorchMode.STROBE);
                    break;
            }
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        Cat.d("Created");

        cameraManager = getSystemService(CameraManager.class);
        cameraManager.registerTorchCallback(torchCallback, null);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        cameraManager.unregisterTorchCallback(torchCallback);

        super.onDestroy();
        INSTANCE = null;
        Cat.d("Destroyed");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean flashAvailable() throws CameraAccessException {
        for (String cameraId : cameraManager.getCameraIdList()) {
            CameraCharacteristics camChars = cameraManager.getCameraCharacteristics(cameraId);
            boolean hasFlash = camChars.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
            if (hasFlash) return true;
        }
        return false;
    }

    private String getCameraIdWithFlash() throws CameraAccessException {
        for (String cameraId : cameraManager.getCameraIdList()) {
            CameraCharacteristics camChars = cameraManager.getCameraCharacteristics(cameraId);
            boolean hasFlash = camChars.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
            if (hasFlash) return cameraId;
        }
        return "";
    }

    private void _turnTorchOn(TorchMode mode) {
        if (isTorchOn) {
            try {
                cameraManager.setTorchMode(getCameraIdWithFlash(), false);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        isTorchOn = true;
        torchMode = mode;
        try {
            cameraManager.setTorchMode(getCameraIdWithFlash(), true);
        } catch (Exception e) {
            Cat.e("Unable to start torch, exception: " + e.getMessage());
        }
        if (torchMode != TorchMode.FLASH_LIGHT) {
            sendBroadcast(new Intent(TORCH_STARTED));
            // in the case for torchMode FLASH_LIGHT, the broadcast send torch callback code
        }
    }

    static void turnTorchOn(TorchMode mode) {
        if (INSTANCE != null) INSTANCE._turnTorchOn(mode);
    }

    private void _turnTorchOff() {
        try {
            isTorchOn = false;
            torchMode = TorchMode.FLASH_LIGHT;
            cameraManager.setTorchMode(getCameraIdWithFlash(), false);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    static void turnTorchOff() {
        if (INSTANCE != null) INSTANCE._turnTorchOff();
    }

    private void _toggleTorch() {
        if (isTorchOn) turnTorchOff();
        else turnTorchOn(TorchMode.FLASH_LIGHT);
    }

    static void toggleTorch() {
        INSTANCE._toggleTorch();
    }

    static boolean isTorchOn() {
        if (INSTANCE == null) {
            return false;
        }
        return INSTANCE.isTorchOn;
    }

    static boolean isTorchOn(TorchMode mode) {
        if (INSTANCE == null) {
            return false;
        }
        return INSTANCE.isTorchOn && INSTANCE.torchMode == mode;
    }

    static TorchMode getTorchMode() {
        if (INSTANCE == null) {
            return TorchMode.FLASH_LIGHT;
        }
        return INSTANCE.torchMode;
    }
}

package be.locpiapps.powertorch

import android.content.Context
import android.os.VibrationEffect
import android.os.VibrationEffect.DEFAULT_AMPLITUDE
import android.os.Vibrator
import androidx.core.content.getSystemService
import be.locpiapps.powertorch.Prefs.VibrateSetting.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import net.vrallev.android.cat.Cat

private const val SHORT_ON_TIME = 100L
private const val SHORT_OFF_TIME = 50L

private const val LONG_ON_TIME = 400L
private const val LONG_OFF_TIME = 200L

private val CONTINUES_PATTERN = longArrayOf(0L, 100L, 500L)

class VibrateServant(appContext: Context) {

    init {
        MainScope().launch {
            torch.modeChannel.consumeEach { mode ->
                Cat.d("Need to update vibrator")
                try {
                    val vibrator = appContext.getSystemService<Vibrator>() ?: return@consumeEach
                    vibrator.cancel()
                    if (prefs.vibrateSetting == NONE) return@consumeEach
                    val vibrateEffect = if (mode != Torch.Mode.OFF) {
                        when (prefs.vibrateSetting) {
                            NONE -> null
                            SHORT -> VibrationEffect.createOneShot(SHORT_ON_TIME, DEFAULT_AMPLITUDE)
                            LONG -> VibrationEffect.createOneShot(LONG_ON_TIME, DEFAULT_AMPLITUDE)
                            CONTINUES -> VibrationEffect.createWaveform(CONTINUES_PATTERN, 0)
                        }
                    } else {
                        when (prefs.vibrateSetting) {
                            NONE -> null
                            SHORT -> VibrationEffect.createOneShot(
                                SHORT_OFF_TIME,
                                DEFAULT_AMPLITUDE
                            )
                            LONG -> VibrationEffect.createOneShot(LONG_OFF_TIME, DEFAULT_AMPLITUDE)
                            CONTINUES -> {
                                vibrator.cancel(); null
                            }
                        }
                    }
                    vibrator.vibrate(vibrateEffect)
                } catch (ignored: Exception) {
                    Cat.e("Failed to consume torch event.")
                }

            }
        }
    }
}
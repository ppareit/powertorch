package be.locpiapps.powertorch

import android.accessibilityservice.AccessibilityService
import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.Build
import android.os.PowerManager
import android.view.accessibility.AccessibilityEvent
import be.locpiapps.powertorch.Torch.Mode.FLASH_LIGHT
import kotlinx.coroutines.*
import net.vrallev.android.cat.Cat

class PbService : AccessibilityService() {

    companion object {
        private var INSTANCE: PbService? = null

        val isRunning: Boolean
            get() = INSTANCE != null
    }

    private val backgroundScope = CoroutineScope(Job() + Dispatchers.Main)

    // keep track when the user logged into the device
    private var mLastTimeUserPresent = 0L

    // keep track when the user last long pressed while logged into the device
    private var mLastTimeGlobalAction = 0L

    // this receiver is triggered when the user logs into the system
    private var mUserPresentReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Cat.d("User is now present")
            mLastTimeUserPresent = System.currentTimeMillis()
        }
    }

    /**
     * @return true if user is calling
     */
    private val isCallActive: Boolean
        get() {
            val manager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            return (manager.mode == AudioManager.MODE_IN_CALL //
                    || manager.mode == AudioManager.MODE_RINGTONE)
        }

    override fun onCreate() {
        Cat.d("Create")

        INSTANCE = this

        val userPresentFilter = IntentFilter(Intent.ACTION_USER_PRESENT)
        registerReceiver(mUserPresentReceiver, userPresentFilter)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return if (prefs.enablePowerButtonToggle) START_STICKY else START_NOT_STICKY
    }

    override fun onDestroy() {
        Cat.d("Destroyed")
        unregisterReceiver(mUserPresentReceiver)
        backgroundScope.cancel()

        INSTANCE = null

        super.onDestroy()
    }

    override fun onServiceConnected() {
        Cat.i("ServiceConnected")
    }

    override fun onInterrupt() {
        Cat.i("Interrupt")
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        Cat.i("AccessibilityEvent")
        if (event == null || event.packageName == null || event.className == null)
            return

        if (prefs.enablePowerButtonToggle == false)
            return

        if (event.className.contains("com.android.systemui.globalactions")) {
            Cat.d("Fire away!")
            // only work our magic while in the lock screen
            // but if we where recent (3 seconds) in the lock screen, still work
            val km = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            if (!km.inKeyguardRestrictedInputMode()
                    && System.currentTimeMillis() - mLastTimeUserPresent > 5000) {
                Cat.d("Unlocked and not recently in keyguard")
                if (torch.torchOn) {
                    Cat.v("Torch running, so in any case stop torch and exit here")
                    closePowerMenu()
                    torch.turnTorchOff()
                } else if (Math.abs(System.currentTimeMillis() - mLastTimeGlobalAction) < 5000) {
                    Cat.v("This is the second time in a short while that the user pressed long")
                    closePowerMenu()
                    torch.changeTorchMode(FLASH_LIGHT)
                } else {
                    Cat.v("User long pressed, start monitoring time")
                    mLastTimeGlobalAction = System.currentTimeMillis()
                }
                return
            }

            val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
            if (!pm.isScreenOn) {
                Cat.d("Screen not on, ignoring")
                return
            }

            closePowerMenu()

            // check if we are not in a call
            if (isCallActive) {
                Cat.d("User is in a call, ignoring")
                return
            }
            torch.toggleTorch()
        }
    }

    @SuppressLint("MissingPermission")
    private fun closePowerMenu() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            val closeDialog = Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)
            sendBroadcast(closeDialog)
            // Just to make sure, wait a bit and remove power menu again
            backgroundScope.launch {
                delay(200)
                sendBroadcast(closeDialog)
            }
        } else {
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
        }
    }

}
package be.locpiapps.powertorch

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import net.vrallev.android.cat.Cat
import org.koin.java.KoinJavaComponent.inject


val prefs: Prefs by inject(Prefs::class.java)

class Prefs(private val appContext: Context) {

    var consentForStrobe: Boolean
        get() = sharedPreferences.getBoolean("consent_for_strobe", false)
        set(newValue) = sharedPreferences.edit { putBoolean("consent_for_strobe", newValue) }

    val enablePowerButtonToggle: Boolean
        get() = sharedPreferences.getBoolean("powerbutton_preference", true)

    enum class VibrateSetting {
        NONE, SHORT, LONG, CONTINUES
    }

    val vibrateSetting: VibrateSetting
        get() {
            val vib = sharedPreferences.getString("vibrate_preference", "SHORT")
            return when (vib) {
                "NONE" -> VibrateSetting.NONE
                "SHORT" -> VibrateSetting.SHORT
                "LONG" -> VibrateSetting.LONG
                "CONTINUES" -> VibrateSetting.CONTINUES
                else -> {
                    Cat.e("Vibrate setting is messed up, returning default")
                    VibrateSetting.SHORT
                }
            }
        }

    val timeoutInMilliseconds: Long
        get() {
            val timeoutValue = sharedPreferences.getString("timeout_preference", "120")
            var timeout = timeoutValue?.toLong() ?: 120
            timeout *= 1000 // move to milliseconds
            return timeout
        }

    val shouldTimeout: Boolean
        get() = timeoutInMilliseconds > 5000 // take at least 5 seconds

    private fun getResourceString(id: Int): String = appContext.resources.getString(id)

    private val sharedPreferences: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)
}
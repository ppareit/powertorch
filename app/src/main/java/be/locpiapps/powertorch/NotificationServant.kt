package be.locpiapps.powertorch

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.VISIBILITY_PUBLIC
import androidx.core.content.getSystemService
import androidx.media.app.NotificationCompat.MediaStyle
import be.locpiapps.powertorch.Torch.Mode
import be.locpiapps.powertorch.TorchReceiver.Action
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach

private const val NOTIFICATION_ID = 456
private const val CHANNEL_ID = "be.ppareit.powertorch"

class NotificationServant(appContext: Context) : ContextWrapper(appContext) {

    private val backgroundScope = CoroutineScope(Job() + Dispatchers.Main)

    private val notificationManager = getSystemService<NotificationManager>()!!

    init {
        createNotificationChannel()

        backgroundScope.launch {
            var job: Job? = null
            torch.modeChannel.consumeEach { mode ->
                job?.cancel()
                job = when (mode) {
                    Mode.OFF -> backgroundScope.launch {
                        val notification = createNotification(R.string.notification_text_stopped)
                        notificationManager.notify(NOTIFICATION_ID, notification)
                        delay(3000)
                        if (job?.isCancelled != true) {
                            notificationManager.cancel(NOTIFICATION_ID)
                        }
                    }
                    Mode.FLASH_LIGHT -> backgroundScope.launch {
                        val notification = createNotification(R.string.notification_text_burning)
                        notificationManager.notify(NOTIFICATION_ID, notification)
                    }
                    else -> null
                }
            }
        }
    }

    private fun createNotification(notificationTextId: Int): Notification {
        val text = resources.getString(notificationTextId)
        val title = resources.getString(R.string.app_name)

        val prefIntent = Intent(this, MainActivity::class.java)
        val prefPendingIntent = PendingIntent.getActivity(
            this, 0,
            prefIntent, FLAG_ACTIVITY_NEW_TASK or FLAG_IMMUTABLE)

        val startAction = NotificationCompat.Action(
            R.drawable.torch_flashlight,
            resources.getString(R.string.flashlight),
            TorchReceiver.createPendingIntent(Action.START_TORCH)
        )
        val stopAction = NotificationCompat.Action(
            R.drawable.torch_off,
            resources.getString(R.string.stop_torch),
            TorchReceiver.createPendingIntent(Action.STOP_TORCH)
        )
        val strobeAction = NotificationCompat.Action(
            R.drawable.torch_strobe,
            resources.getString(R.string.strobe),
            TorchReceiver.createPendingIntent(Action.START_STROBE)
        )

        val notification = with(NotificationCompat.Builder(this, CHANNEL_ID)) {
            setContentTitle(title)
            setContentText(text)
            setSmallIcon(R.mipmap.notification)
            setAutoCancel(false)
            setOngoing(true)
            setContentIntent(prefPendingIntent)
            setShowWhen(false)
            setVisibility(VISIBILITY_PUBLIC)

            if (PtSettings.getConsentForStrobe()) {
                addAction(strobeAction)
            }
            addAction(startAction)
            addAction(stopAction)

            if (PtSettings.getConsentForStrobe()) {
                setStyle(MediaStyle().setShowActionsInCompactView(0, 1, 2))
            } else {
                setStyle(MediaStyle().setShowActionsInCompactView(0, 1))
            }

            build()
        }

        // hackish way to make sure we only show collapsed notification
        notification.bigContentView = null
        return notification
    }

    private fun createNotificationChannel() {
        val channelName = "Powertorch notifications"
        val channelDescription = "Shows a notification while the torch is burning"
        val channelImportance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(CHANNEL_ID, channelName, channelImportance)
        channel.description = channelDescription
        notificationManager.createNotificationChannel(channel)
    }

}

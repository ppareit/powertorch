package be.locpiapps.powertorch

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import be.locpiapps.android.ButtonPreference
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch


private const val REQUEST_BILLING = 15

class PrefFragment :
        PreferenceFragmentCompat(),
        SharedPreferences.OnSharedPreferenceChangeListener {

    private val torchSwitchPreference by lazy {
        findPreference<SwitchPreference>("torch_switch")!!
    }
    private val strobeSwitchPreference by lazy {
        findPreference<SwitchPreference>("strobe_switch")!!
    }
    private val accessibilityButton: ButtonPreference by lazy {
        findPreference<ButtonPreference>("accessibility_button")!!
    }
    private val payPreference by lazy {
        findPreference<Preference>("pay_preference")!!
    }
    private val timeoutPreference: Preference by lazy {
        findPreference<Preference>("timeout_preference")!!
    }

    private val vibrationPreference: Preference by lazy {
        findPreference("vibrate_preference")!!
    }
    private val powerButtonCheckBoxPreference: CheckBoxPreference by lazy {
        findPreference<CheckBoxPreference>("powerbutton_preference")!!
    }
    private val lockscreenInfoPreference: Preference by lazy {
        findPreference<Preference>("lockscreen_info")!!
    }
    private val powermenuInfoPreference: Preference by lazy {
        findPreference<Preference>("powermenu_info")!!
    }


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.main_preferences, rootKey)

        torchSwitchPreference.setOnPreferenceChangeListener { preference, newValue ->
            if (newValue as Boolean) {
                torch.changeTorchMode(Torch.Mode.FLASH_LIGHT)
            } else {
                torch.turnTorchOff()
            }
            return@setOnPreferenceChangeListener true
        }
        MainScope().launch {
            torch.modeChannel.consumeEach { mode ->
                torchSwitchPreference.isChecked = mode == Torch.Mode.FLASH_LIGHT
            }
        }

        strobeSwitchPreference.setOnPreferenceChangeListener { preference, newValue ->
            if (newValue as Boolean) {
                torch.changeTorchMode(Torch.Mode.STROBE)
            } else {
                torch.turnTorchOff()
            }
            return@setOnPreferenceChangeListener true
        }
        MainScope().launch {
            torch.modeChannel.consumeEach { mode ->
                strobeSwitchPreference.isChecked = mode == Torch.Mode.STROBE
            }
        }

        accessibilityButton.onButtonClickListener = View.OnClickListener {
            val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
            startActivity(intent)
        }

        payPreference.setOnPreferenceClickListener {
            AlertDialog.Builder(activity)
                    .setTitle("Upgrade from demo")
                    .setMessage("Next version will allow you to upgrade.")
                    .setCancelable(true)
                    .create()
                    .show()
            true
        }

        timeoutPreference.setOnPreferenceChangeListener { preference, newValue ->
            torch.turnTorchOff()
            return@setOnPreferenceChangeListener true
        }

        vibrationPreference.setOnPreferenceChangeListener { preference, newValue ->
            torch.turnTorchOff()
            return@setOnPreferenceChangeListener true
        }

    }

    override fun onResume() {
        super.onResume()
        updateUI()
        preferenceManager.sharedPreferences?.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceManager.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        updateUI()
    }

    private fun updateUI() {
        if (PbService.isRunning) {
            accessibilityButton.buttonText = getString(R.string.accessibility_ok)
            powerButtonCheckBoxPreference.isEnabled = true
            powerButtonCheckBoxPreference.summary = getString(R.string.powerbutton_summary)
        } else {
            accessibilityButton.buttonText = getString(R.string.accessibility_fix)
            powerButtonCheckBoxPreference.isEnabled = false
            powerButtonCheckBoxPreference.summary = getString(R.string.powerbutton_summary_when_disabled)
        }
        lockscreenInfoPreference.isVisible = PbService.isRunning && prefs.enablePowerButtonToggle
        powermenuInfoPreference.isVisible = PbService.isRunning && prefs.enablePowerButtonToggle
    }

}



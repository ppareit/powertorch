package be.locpiapps.powertorch

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.util.Linkify
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import be.locpiapps.android.ToastBuilder


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity_layout)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings_container, PrefFragment())
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_feedback -> {
                val to = "pieter.pareit@gmail.com"
                val subject = "Power Torch feedback"
                val message = """
                Device: ${Build.MODEL}
                Android version: ${Build.VERSION.RELEASE}
                Feedback: 
                
                """.trimIndent()
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
                emailIntent.putExtra(Intent.EXTRA_TEXT, message)
                emailIntent.type = "message/rfc822"
                try {
                    startActivity(emailIntent)
                } catch (exception: ActivityNotFoundException) {
                    ToastBuilder(this)
                            .setText("Error: unable to start mail client.")
                            .setDuration(ToastBuilder.Duration.LONG)
                            .build()
                            .show()
                }
            }
            R.id.action_about -> {
                val ad: AlertDialog = AlertDialog.Builder(this)
                        .setTitle(R.string.about_dlg_title)
                        .setMessage(R.string.about_dlg_message)
                        .setPositiveButton(android.R.string.ok, null)
                        .create()
                ad.show()
                Linkify.addLinks(ad.findViewById<TextView>(android.R.id.message)!!, Linkify.ALL)
            }
        }
        return true
    }

}
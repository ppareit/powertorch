package be.locpiapps.powertorch

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import be.locpiapps.android.AppBase
import net.vrallev.android.cat.Cat
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

class App : AppBase() {

    class BootCompletedReceiver : BroadcastReceiver() {
        @SuppressLint("UnsafeProtectedBroadcastReceiver")
        override fun onReceive(context: Context, intent: Intent) {
            Cat.d("We have been started successfully!")
            // nothing more to do, the onCreate of this app will
            // start the important services
        }
    }

    override fun onCreate() {
        super.onCreate()
        Cat.d("onCreate called")

        val appModule = module {
            single { this@App }
            singleOf(::Torch)
            singleOf(::Prefs)
            singleOf(::TorchReceiver)
            singleOf(::TimeoutServant)
            singleOf(::VibrateServant)
            singleOf(::NotificationServant)
        }

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule)
        }

        get<NotificationServant>()
        get<TorchReceiver>()
        get<TimeoutServant>()
        get<VibrateServant>()
        get<NotificationServant>()
    }
}

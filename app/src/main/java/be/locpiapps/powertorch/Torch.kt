package be.locpiapps.powertorch

import android.content.Context
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraManager.TorchCallback
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import net.vrallev.android.cat.Cat
import org.koin.java.KoinJavaComponent.get


val torch: Torch = get(Torch::class.java)

class Torch(val context: Context) {

    enum class Mode {
        OFF,
        FLASH_LIGHT,
        STROBE,
    }

    var mode: Mode = Mode.OFF

    val modeChannel = ConflatedBroadcastChannel<Mode>()

    private val cameraManager by lazy {
        context.getSystemService(CameraManager::class.java)!!
    }

    private var torchCallback: TorchCallback = object : TorchCallback() {
        override fun onTorchModeChanged(cameraId: String, enabled: Boolean) {
            Cat.d("Torch mode changed: enabled = $enabled")
            if (mode == Mode.OFF && enabled == true) {
                // some other program, for instance android system has just turned on the flashlight
                mode = Mode.FLASH_LIGHT
                modeChannel.trySend(mode)
            } else if (mode == Mode.FLASH_LIGHT && enabled == false) {
                // some other program just turned off the flashlight
                mode = Mode.OFF
                modeChannel.trySend(mode)
            } else if (mode == Mode.STROBE) {
                // we need to strobe
                if (enabled) {
                    cameraManager.setTorchMode(cameraWithFlashId, false)
                    Thread.sleep(90)
                } else {
                    cameraManager.setTorchMode(cameraWithFlashId, true)
                    Thread.sleep(10)
                }
            }
        }

        override fun onTorchModeUnavailable(cameraId: String) {
            Cat.d("Torch mode no longer available, maybe stop torch")
            if (mode != Mode.OFF) {
                mode = Mode.OFF
                modeChannel.trySend(mode)
            }
        }
    }

    init {
        Cat.d("Torch initializing")
        cameraManager.registerTorchCallback(torchCallback, null)
        MainScope().launch {
            modeChannel.consumeEach {
                Cat.d("Mode to ${it.name}")
            }
        }
    }

    fun changeTorchMode(mode: Mode) {
        if (flashAvailable() == false) {
            return
        }
        when (mode) {
            this.mode -> return
            Mode.OFF -> {
                cameraManager.setTorchMode(cameraWithFlashId, false)
            }
            Mode.FLASH_LIGHT -> {
                cameraManager.setTorchMode(cameraWithFlashId, true)
            }
            Mode.STROBE -> {
                cameraManager.setTorchMode(cameraWithFlashId, true)
            }
        }
        this.mode = mode
        modeChannel.trySend(mode)
    }

    fun turnTorchOn() {
        changeTorchMode(Mode.FLASH_LIGHT)
    }

    fun turnTorchOff() {
        changeTorchMode(Mode.OFF)
    }

    fun toggleTorch() {
        changeTorchMode(if (torchOn) Mode.OFF else Mode.FLASH_LIGHT)
    }

    val torchOn: Boolean
        get() = mode != Mode.OFF

    private fun flashAvailable(): Boolean {
        try {
            for (cameraId in cameraManager.cameraIdList) {
                val camChars = cameraManager.getCameraCharacteristics(cameraId)
                val hasFlash = camChars.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)
                if (hasFlash == true) return true
            }
        } catch (e: Exception) {
            Cat.w(e.message)
        }
        return false
    }

    private val cameraWithFlashId: String
        get() {
            try {
                for (cameraId in cameraManager.cameraIdList) {
                    val camChars = cameraManager.getCameraCharacteristics(cameraId)
                    val hasFlash = camChars.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)
                    if (hasFlash == true) return cameraId
                }
            } catch (e: Exception) {
                Cat.w(e)
            }
            return ""
        }

}




























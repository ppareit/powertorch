package be.locpiapps.powertorch

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach

class TimeoutServant {

    init {
        MainScope().launch {
            torch.modeChannel.consumeEach { mode ->
                if (prefs.shouldTimeout == false) return@consumeEach
                if (mode != Torch.Mode.OFF) {
                    startTimeout()
                } else {
                    stopTimeout()
                }
            }
        }
    }

    lateinit var job: Job

    private fun startTimeout() {
        job = MainScope().launch {
            delay(prefs.timeoutInMilliseconds)
            if (isActive) {
                torch.turnTorchOff()
            }
        }
    }

    private fun stopTimeout() {
        if (::job.isInitialized)
            job.cancel()
    }

}
